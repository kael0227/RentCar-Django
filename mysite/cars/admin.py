from django.contrib import admin
from cars.models import Car, TypeGas, CarBrand, CarType, CarModel, Rent, Inspection
from import_export.admin import ImportExportModelAdmin
from django import forms
# Register your models here.
class CarBrandAdmin(admin.ModelAdmin):
    pass
admin.site.register(CarBrand, CarBrandAdmin)

class CarModelAdmin(admin.ModelAdmin):
    pass
admin.site.register(CarModel, CarModelAdmin)

class CarTypeAdmin(admin.ModelAdmin):
    pass
admin.site.register(CarType, CarTypeAdmin)

class TypeGasAdmin(admin.ModelAdmin):
    pass
admin.site.register(TypeGas, TypeGasAdmin)

class CarForm(forms.ModelForm):
    car_id = forms.CharField(min_length=7, max_length=7)
    no_chassis = forms.CharField(min_length=17,max_length=17)
    no_engine = forms.CharField(min_length=8, max_length=8)

    class meta:
        model = Car

class CarAdmin(admin.ModelAdmin):
    form = CarForm
    pass
admin.site.register(Car, CarAdmin)

class InspectionAdmin(admin.ModelAdmin):
    pass
admin.site.register(Inspection,InspectionAdmin)

class RentAdmin(admin.ModelAdmin):
    pass
admin.site.register(Rent, RentAdmin)


