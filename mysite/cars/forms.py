from django import forms
from .models import Inspection, Rent, Car

class InspectionForm(forms.ModelForm):
    class Meta:
        model = Inspection
        fields = ['scratches', 'gasoline', 'spare_tire', 'gato', 'tires', 'observation', 'date', 'car', 'client', 'employee']
        widgets = {
            'date': forms.DateTimeInput(attrs={'class': 'datetime-input'})
        },
        

        
    


class RentForm(forms.ModelForm):
    class Meta:
        model = Rent
        fields = ['rent_date', 'devolution_date', 'amount_per_day','days','comments','employee','car','client']