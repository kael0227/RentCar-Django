# Generated by Django 2.0 on 2018-05-17 03:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('car_id', models.CharField(max_length=80, verbose_name='CarID')),
                ('description', models.CharField(max_length=80, verbose_name='Description')),
                ('no_chassis', models.CharField(max_length=80, verbose_name='Chassis')),
                ('no_engine', models.CharField(max_length=80, verbose_name='Engine')),
                ('img', models.ImageField(upload_to='', verbose_name='CarImage')),
                ('status', models.BooleanField(default=False, verbose_name='Status')),
            ],
        ),
        migrations.CreateModel(
            name='CarBrand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=80, verbose_name='Description')),
                ('status', models.BooleanField(default=False, verbose_name='Status')),
            ],
        ),
        migrations.CreateModel(
            name='CarModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=80, verbose_name='Description')),
                ('status', models.BooleanField(default=False, verbose_name='Status')),
                ('car_brand', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cars.CarBrand')),
            ],
        ),
        migrations.CreateModel(
            name='CarType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=80, verbose_name='Description')),
                ('status', models.BooleanField(default=False, verbose_name='Status')),
            ],
        ),
        migrations.CreateModel(
            name='TypeGas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=80, verbose_name='Description')),
                ('status', models.BooleanField(default=False, verbose_name='Status')),
            ],
        ),
        migrations.AddField(
            model_name='car',
            name='car_model',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cars.CarModel'),
        ),
        migrations.AddField(
            model_name='car',
            name='car_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cars.CarType'),
        ),
        migrations.AddField(
            model_name='car',
            name='type_gas',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cars.TypeGas'),
        ),
    ]
