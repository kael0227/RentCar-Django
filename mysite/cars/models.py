from django.db import models
import datetime
from employees.models import Employee
from clients.models import Client
from . import validators

# Create your models here.



class CarBrand(models.Model):
    description = models.CharField(max_length=80, verbose_name='Description')
    status = models.BooleanField(default=False, verbose_name='Inactive')

    def __str__(self):
        return self.description

class CarModel(models.Model):
    car_brand = models.ForeignKey(CarBrand, on_delete=models.CASCADE)
    description = models.CharField(max_length=80, verbose_name='Description')
    status = models.BooleanField(default=False, verbose_name='Inactive')

    def __str__(self):
        return self.description

class CarType(models.Model):
    description = models.CharField(max_length=80, verbose_name='Description')
    status = models.BooleanField(default=False, verbose_name='Inactive')

    def __str__(self):
        return self.description

class TypeGas(models.Model):
    description = models.CharField(max_length=80, verbose_name='Description')
    status = models.BooleanField(default=False, verbose_name='Inactive')

    def __str__(self):
        return self.description

class Car(models.Model):
    car_id = models.CharField(max_length=7, verbose_name='CarID', validators=[validators.alphanumeric])
    description = models.CharField(max_length=80, verbose_name='Description', validators=[validators.alphanumeric])
    no_chassis = models.CharField(max_length=17, verbose_name='Chassis', validators=[validators.alphanumeric])
    no_engine = models.CharField(max_length=8, verbose_name='Engine', validators=[validators.alphanumeric])
    car_type = models.ForeignKey(CarType, on_delete=models.CASCADE)
    car_model = models.ForeignKey(CarModel, on_delete=models.CASCADE)
    type_gas = models.ForeignKey(TypeGas, on_delete=models.CASCADE)
    img = models.ImageField(verbose_name='CarImage', upload_to='static/img')
    status = models.BooleanField(default=False, verbose_name='Inactive')


    def __str__(self):
        return self.car_id

class Rent(models.Model):
    rent_date = models.DateField(default=datetime.date.today, verbose_name='Rent Date')
    devolution_date = models.DateField(default=datetime.date.today, verbose_name='Devolution Date')
    amount_per_day = models.FloatField(default=0.0, verbose_name='Amount per Day')
    days = models.IntegerField(verbose_name='Days')
    comments = models.CharField(max_length=80, verbose_name='Comments')
    employee = models.ForeignKey(Employee,on_delete=models.CASCADE)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    status = models.BooleanField(default=False, verbose_name='Inactive')

    def __str__(self):
        return self.client.name 

class Inspection(models.Model):
    scratches = models.BooleanField(default=False, verbose_name='Scratches')
    gasoline = models.CharField(max_length=80, verbose_name = 'Gasoline')
    spare_tire = models.BooleanField(default=False, verbose_name='Spare Tire')
    gato = models.BooleanField(default=False, verbose_name='Gato')
    tires = models.IntegerField(verbose_name='Tires')
    observation = models.TextField(max_length=180, verbose_name='Observation')
    date = models.DateField(default=datetime.date.today, verbose_name='Date')
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    status = models.BooleanField(default=False)

    



    def __str__(self):
        return self.client.name

class Meta:
    verbose_name='Car'
    verbose_name_plural='Cars'

