from django.conf.urls import url, include
from django.urls import path
from . import views

app_name='cars'

urlpatterns = [
    # home/
    path('', views.Index.as_view(), name='index'),
    #rentcar/
    path('rentcar/', views.Rentcar.as_view(), name='rentcar'),

    path('rentcar/<int:pk>/', views.RentcarDetailView.as_view(), name='rentcar-detail'),

    path('inspection-form/', views.InspectionCreate.as_view(), name='create_inspection' ),

    path('rent-form/', views.RentCreate.as_view(), name='create_rent' ),

    path('rent/', views.RentList.as_view(), name='rent'),

    path('rent/', views.RentListP.as_view(), name='rentpost')  

]