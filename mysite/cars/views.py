from django.shortcuts import render, redirect
from django.views.generic import TemplateView, ListView, DetailView, CreateView
from cars.forms import InspectionForm, RentForm
from cars.models import Car, Rent
from django.urls import reverse
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from .util import RentResources
from django.http import HttpResponse


# Create your views here.
class Index(TemplateView):
    template_name = "cars/home.html"

class Rentcar(ListView):
    template_name = "cars/rentcar.html"
    model = Car
    paginate_by = 3
    queryset = Car.objects.filter(status=False)




   
class RentListP(ListView):
    template_name = "cars/rent.html"
    model = Rent
    success_url = '/rent/' 

    def get_context_data(self, **kwargs):

        context = super(RentListP, self).get_context_data(**kwargs)

        
        
     

        id = self.request.GET['rentid']
        rent = Rent.objects.get(pk=id)
        print(rent)
        
        context['url'] = reverse('cars:rentpost' )
        return context

    def get_success_url(self):
        rentid = self.request.session['rentid']
        rent = Rent.objects.get(pk=rentid) 
        rent.status = True
        rent.save()
        return self.success_url

       


class RentList(ListView):
    template_name = "cars/rent.html"
    model = Rent
    success_url = '/rent/'
    

    def get_context_data(self, **kwargs):

        context = super(RentList, self).get_context_data(**kwargs)
        try:
            date1 = self.request.GET['date1']
            date2 = self.request.GET['date2']
            if date1 > date2:
                context['message'] = 'datebeg cant be higher than dateend'
            queryset = Rent.objects.filter(rent_date__range=[date1, date2])
            context['object_list'] = queryset
        
        except Exception as e:
            print(e)

        
        context['url'] = reverse('cars:rent' )
        return context

    
    


class RentcarDetailView(DetailView):
    model = Car
    

    def get_object(self):
        object = super().get_object()
        return object


class InspectionCreate(CreateView):
    model = Car
    form_class = InspectionForm
    template_name = 'cars/template-form.html'
    success_url = '/rent-form/'

    def get_object(self):
        object = super().get_object()
        return object

    def get_context_data(self, **kwargs):

        context = super(InspectionCreate, self).get_context_data(**kwargs)
        car_id = self.request.GET['car_id']
        car = Car.objects.get(pk=car_id) 
        self.request.session['car_id'] = car_id
        print(car)
        context['url'] = reverse('cars:create_inspection' )
        context['name'] = 'Inspect your car'
        context['form'] = InspectionForm(initial={'car': car})
        return context
    
    def get_success_url(self):
        car_id = self.request.session['car_id']
        success_url = '/rent-form/?car_id={0}'.format(car_id) 

        return success_url



class RentCreate(CreateView):
    model = Car
    form_class = RentForm
    template_name = 'cars/template-form.html'
    success_url = '/rentcar/'

    def get_object(self):
        object = super().get_object()
        return object

    def get_context_data(self, **kwargs):
        context = super(RentCreate, self).get_context_data(**kwargs)

        car_id = self.request.GET['car_id']
        car = Car.objects.get(pk=car_id) 
        self.request.session['car_id'] = car_id
        print(car)
        context['url'] = reverse('cars:create_rent')
        context['name'] = 'Rent process'
        context['form'] = RentForm(initial={'car': car})
        return context

    def get_success_url(self):
        car_id = self.request.session['car_id']
        car = Car.objects.get(pk=car_id) 
        car.status = True
        car.save()
        return self.success_url


        
       


