from django.contrib import admin
from clients.models import Client, PersonType
from django import forms
# Register your models here.



class ClientForm(forms.ModelForm):
    nationalid = forms.CharField(min_length=11,max_length=11)
    no_card = forms.CharField(min_length=16, max_length=16)

    class meta:
        model = Client

class ClientAdmin(admin.ModelAdmin):
    form = ClientForm
    pass

admin.site.register(Client, ClientAdmin)

class PersonTypeAdmin(admin.ModelAdmin):
    pass
admin.site.register(PersonType, PersonTypeAdmin)