# Generated by Django 2.0 on 2018-06-12 00:45

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0002_auto_20180611_2041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='no_card',
            field=models.CharField(max_length=16, validators=[django.core.validators.RegexValidator('^[0-9]*$', 'Only numeric characters are allowed.')], verbose_name='Card'),
        ),
    ]
