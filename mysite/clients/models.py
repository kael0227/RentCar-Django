from django.db import models
from . import validators

# Create your models here.

class PersonType(models.Model):
    description = models.CharField(max_length=80, verbose_name='Description')




    def __str__(self):
        return self.description


class Client(models.Model):
    name = models.CharField(max_length=80, verbose_name='Name', validators=[validators.letters])
    nationalid = models.CharField(max_length=11, verbose_name='National ID', validators=[validators.numeric])
    no_card = models.CharField(max_length=16,verbose_name='Card', validators=[validators.numeric])
    credit_limit = models.FloatField(default=0.0, verbose_name='Credit limit')
    person_type = models.ForeignKey(PersonType, on_delete=models.CASCADE)
    status = models.BooleanField(default=False, verbose_name='Inactive')


    def __str__(self):
        return self.name 
