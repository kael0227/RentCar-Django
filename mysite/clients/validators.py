from django.core.validators import RegexValidator

numeric = RegexValidator(r'^[0-9]*$', 'Only numeric characters are allowed.')

letters = RegexValidator(r'^[a-zA-Z ]*$', 'Only letters characters are allowed.')