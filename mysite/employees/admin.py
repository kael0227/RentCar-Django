from django.contrib import admin
from employees.models import Employee, Schedule
from django import forms
# Register your models here.

class EmployeeForm(forms.ModelForm):
    nationalid = forms.CharField(min_length=11,max_length=11)

    class meta:
        model = Employee

class EmployeesAdmin(admin.ModelAdmin):
    form = EmployeeForm
    pass
admin.site.register(Employee, EmployeesAdmin)

class ScheduleAdmin(admin.ModelAdmin):
    pass
admin.site.register(Schedule, ScheduleAdmin)
