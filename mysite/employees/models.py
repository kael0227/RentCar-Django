from django.db import models
import datetime
from . import validators

# Create your models here.

class Schedule(models.Model):
    description = models.CharField(max_length=80, verbose_name='Description')
    status = models.BooleanField(default=False, verbose_name='Inactive')

    def __str__(self):
        return self.description
        
class Employee(models.Model):
    name = models.CharField(max_length=80, verbose_name='Name', validators=[validators.letters])
    nationalid = models.CharField(max_length=11, verbose_name='National ID', validators=[validators.numeric])
    comission = models.FloatField(default=0.0, verbose_name='Comission')
    hiredate = models.DateField(default=datetime.date.today, verbose_name='Hire Date')
    id_schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    status = models.BooleanField(default=False, verbose_name='Inactive')


    def __str__(self):
        return self.name 

